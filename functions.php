<?php
    


    function mostra_categorias() {
        $taxonomy     = 'product_cat';
        $orderby      = 'name';  
        $show_count   = 0;      // 1 for yes, 0 for no
        $pad_counts   = 0;      // 1 for yes, 0 for no
        $hierarchical = 1;      // 1 for yes, 0 for no  
        $title        = '';  
        $empty        = 0;
        
        $args = array(
                'taxonomy'     => $taxonomy,
                'orderby'      => $orderby,
                'show_count'   => $show_count,
                'pad_counts'   => $pad_counts,
                'hierarchical' => $hierarchical,
                'title_li'     => $title,
                'hide_empty'   => $empty
        );

        $all_categories = get_categories( $args );
        foreach ($all_categories as $cat) {
            
            if($cat->category_parent == 0) {
                $category_id = $cat->term_id;   
                $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true ); 
                $image = wp_get_attachment_url( $thumbnail_id );
                echo "<li><img src='{$image}' alt=''  />
                <a class='nome-categoria'href='". get_term_link($cat->slug, 'product_cat') ."'>$cat->name</a></li>";
        
                $args2 = array(
                        'taxonomy'     => $taxonomy,
                        'child_of'     => 0,
                        'parent'       => $category_id,
                        'orderby'      => $orderby,
                        'show_count'   => $show_count,
                        'pad_counts'   => $pad_counts,
                        'hierarchical' => $hierarchical,
                        'title_li'     => $title,
                        'hide_empty'   => $empty
                );
                $sub_cats = get_categories( $args2 );
                if($sub_cats) {
                    foreach($sub_cats as $sub_category) {
                        echo  $sub_category->name ;
                    }   
                }
            }       
        }
    }
    add_action ( 'categorias', 'mostra_categorias');

    function mostra_pratos_dia() {
        setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                    date_default_timezone_set('America/Sao_Paulo');
                    $day=strftime(ucwords("%w",time()));
                    switch ($day) {
                        case 0:
                            echo "<h3>DOMINGO</h3>";
                            break;
                        case 1:
                            echo "<h3>SEGUNDA</h3>";
                            break;
                        case 2:
                            echo "<h3>TERÇA</h3>";
                            break;
                        case 3:
                            echo "<h3>QUARTA</h3>";
                            break;
                        case 4:
                            echo "<h3>QUINTA</h3>";
                            break;
                        case 5:
                            echo "<h3>SEXTA</h3>";
                            break;
                        case 6:
                            echo "<h3>SÁBADO</h3>";
                            break;
                        
                    }
                    $args = array(
                        'post_type'      => 'product',
                        'posts_per_page' => 4,
                        'product_tag'    => $day,
                        'product_id'     => $product_id
                    );
                    echo"<ul class='pratos'>";
                    $loop = new WP_Query( $args );
                
                    while ( $loop->have_posts() ) : $loop->the_post();
                        global $product;
                        
                        $titulo = get_the_title();
                        $preco = wc_price($product->get_price_including_tax(1,$product->get_price()));;
                        global $product;
                        $pid = $product->get_id();
                        do_action( 'aws_reindex_product', $pid );
                        do_action( 'aws_reindex_table' );
                        echo"<li>{$imagem}
                                <div>
                                    <p class='nome-prato'>{$titulo}</p>
                                    <div class='preco-cart'>
                                        <p>{$preco}</p>
                                        <a href ='/cart/?add-to-cart={$pid}'><img src='http://projetofinal.local/wp-content/themes/projeto_final_theme./imgs/black-cart.png'></a>
                                    </div>
                                </div>
                            </li>";
                                
                        endwhile;
                        echo "</ul>";
                wp_reset_query();
    }

function format_products($products){
    $products_final = [];
    foreach ($products as $product) {
        // $products_final[] =[
        //     'name' => $product->get_name(),
        //     'price' => $product->get_price_html(),
        //     'link' => $product->get_permalink(),
            // 'img' => wp_get_attachment_image_src($product->get_image_id(),'medium')[0],
        // ];
    }
    return $products_final;
}
    

?>

   
  