<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php bloginfo('name')?></title>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>./style.css">
    <?php wp_head(); ?>
</head>
<body>
    <header>
        <span>
            <a href="http://projetofinal.local/landing-page/"><img class="header-logo" src="<?php echo get_stylesheet_directory_uri() ?>./imgs/comes_bebes.png" alt=""></a>
            <form id="search-form" role="search" method="get" class="woocommerce-product-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                <div class="caixa-texto">
                    <div class="caixa-botao">
                        <button type="submit" value="<?php echo esc_attr_x( 'Search', 'submit button', 'woocommerce' ); ?>"><img id="lupinha" src="<?php echo get_stylesheet_directory_uri() ?>./imgs/magnifier.png" alt=""></button>
                    </div>
                    <!-- <input type="search" name="s" id="s" value="<?php the_search_query(); ?>" > -->
                    <input type="search" id="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>" class="search-field" placeholder="<?php echo esc_attr__( 'Search products&hellip;', 'woocommerce' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
                    <input type="hidden" name="post_type" value="product" />
                </div>
            </form>
            
        </span>
        <span> 
            <a class="faca-pedido" href="">Faça um pedido</a>
            <input type="checkbox" class="header-icon" id="carrinho-side">
            <label for="carrinho-side">
                <img class="header-icon" src="<?php echo get_stylesheet_directory_uri() ?>./imgs/cart.png" alt="">
            </label>
            <img class="header-icon" src="<?php echo get_stylesheet_directory_uri() ?>./imgs/people_icon.png" alt="">
        
            <?php get_sidebar(); ?>
        </span>
    </header>
