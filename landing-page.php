<?php /* Template Name: Landing Page */ ?>
<?php get_header(); ?>
<section class="banner-landing">
 
    <h1>Comes&Bebes</h1>
    <p>O restaurante para todas as fomes</p>
</section>

<section class="stand-landing">
<h1>CONHEÇA NOSSA LOJA</h1>
    <div class="pratos-principais">
        <h2>Tipos de pratos principais</h2>
        <ul class="categorias">
        <?php do_action('categorias');?>
        </ul>
    </div>
    <div class="pratos-dia">
        <h2>Pratos do dia de hoje</h2>
        <?php do_action('pratos-dia'); ?>
        
        <!-- <?php echo WC()->cart->get_cart_contents_count(); ?>  -->
    </div>
    <a class="outras-opcoes" href="http://projetofinal.local/lista-produtos/">Veja outras opções</a>
</section>
<section class="visite-nossa-loja">
    <h1>VISITE NOSSA LOJA FÍSICA</h1>
   
    <div>
        <div class="contato">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d58809.72461506898!2d-43.1538999!3d-22.8909426!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9983c056e33547%3A0x2f0b8b748690004a!2sCentro%2C%20Niter%C3%B3i%20-%20RJ!5e0!3m2!1spt-BR!2sbr!4v1634075350013!5m2!1spt-BR!2sbr" width="345" height="203" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            <div class="endereco">
                <img  src="<?php echo get_stylesheet_directory_uri() ?>./imgs/talheres.png" alt="">
                <p>endereço endereço endereço</p>
            </div>
            <div class="endereco">
                <img  src="<?php echo get_stylesheet_directory_uri() ?>./imgs/telefone.png" alt="">
                <p>21 9 9999-9999</p>
            </div>
            </div>
        <div class="nossa-loja">
            <img  src="<?php echo get_stylesheet_directory_uri() ?>./imgs/nossa-loja1.png" alt="">
        </div>
    </div>
 

</section>

<?php get_footer(); ?>