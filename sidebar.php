<div id="sidebar">
    <?php
    global $woocommerce;
    $items = $woocommerce->cart->get_cart();
    
    echo "
    
    <h1>CARRINHO</h1>
    <div class='linha-carrinho'></div>
    <input type='checkbox' class='header-icon' id='carrinho-side'>
    <label for='carrinho-side'>
    <p id='x-fechar'>X</p>
    </label>
    ";
    foreach($items as $item => $values) { 
        $image = wp_get_attachment_url( $thumbnail_id );
        $_product =  wc_get_product( $values['data']->get_id() );
            //product image
            $getProductDetail = wc_get_product( $values['product_id'] );
            echo '<div class="caixa-prato-carrinho">';
            echo '<div class="imagens-carrinho">';
            echo $getProductDetail->get_image(); // accepts 2 arguments ( size, attr )
            echo '</div>';
            echo '<div class="texto-carrinho">';
            echo "<p class='nome-prato-carrinho'>".$_product->get_title() .'</p>'; //quantidade de produtos
            $price = get_post_meta($values['product_id'] , '_price', true);
            echo "<div class='quantidade-preco-carrinho'>";
            echo "<p class='quantidade-prato-carrinho'>".$values['quantity']."</p>";
            echo "<p class='preco-prato-carrinho'> R$ ".$price."</p>";
           
            echo '</div>';
            echo '</div>';
            echo '</div>';
        
        
    }
    ?>
    <div class="linha-carrinho"></div>
    <a id="botao-carrinho" href="http://projetofinal.local/checkout">COMPRAR</a>
        </div>
<div id='opacidade'>
</div>